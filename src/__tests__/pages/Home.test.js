import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import Home from '../../pages/Home';


configure({ adapter: new Adapter() });
describe('Home Page', () => {
  it('should render the Home component correctly in "debug" mode', () => {
    const component = shallow(<Home debug />);
    expect(component).toMatchSnapshot();
  });
});



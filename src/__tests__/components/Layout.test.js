import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import Layout from '../../components/Layout';


configure({ adapter: new Adapter() });
describe('Layout', () => {
  it('should render the Layout component correctly in "debug" mode', () => {
    const component = shallow(<Layout debug children />);
    expect(component).toMatchSnapshot();
  });
});




import React from "react";
import Card from "react-bootstrap/Card";
import PropTypes from "prop-types";

function characterItem({ character }) {
  return (
    <div className="col-sm-12 col-md-6 col-lg-3 mt-3">
      <Card>
        <Card.Img
          variant="top"
          alt="character"
          src={`${character.thumbnail.path}.${character.thumbnail.extension}`}
          className="w-100"
          style={{
            maxHeight: "250px",
            objectFit: "cover",
          }}
        />
        <Card.Body
          style={{
            minHeight: "250px",
            maxHeight: "250px",
            overflowY: "auto",
          }}
        >
          <Card.Title>{character.name}</Card.Title>
          <Card.Text>{character.description.trim() || "No description"}</Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
}

characterItem.propTypes = {
  character: PropTypes.object.isRequired,
};
export default characterItem;

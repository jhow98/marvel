import React from "react";
import { Welcome, Container } from "./styles";
import logo from "./../../img/logo.png";
import { Link } from "react-router-dom";
import Image from "react-bootstrap/Image";
import PropTypes from "prop-types";

function Layout({ children }) {
  return (
    <Container>
      <Welcome>
        <Link to={"/"}>
          <Image src={logo} alt="Marvel Logo" />
        </Link>
      </Welcome>
      {children}
    </Container>
  );
}

export default Layout;

Layout.propTypes = {
  children: PropTypes.element.isRequired,
};

import styled from "styled-components";
import constants from "../../utils";

// eslint-disable-next-line import/prefer-default-export
export const Welcome = styled.div`
  background-color: ${constants.CSS_COLORS.PRIMARY_COLOR};
  width: 100%;
  padding: 40px;
  display: block;
  margin: 0 auto;

  img {
    display: block;
    margin: 0 auto;
    max-width: 400px;

    @media (max-width: 724px) {
      width: 100%;
    }
  }
`;

export const Container = styled.div`
  width: 80vw;
  min-height: 100vh;
  display: block;
  margin: 0 auto;

  @media (max-width: 724px) {
    height: auto;
    width: 100vw;
  }
`;

import React from "react";
import Card from "react-bootstrap/Card";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

function comicItem({ comic }) {
  return (
    <div className="col-sm-12 col-md-6 col-lg-3 mt-3">
      <Card>
        <Link to={`/comic/${comic?.id}`}>
          <Card.Img
            variant="top"
            alt="character"
            src={`${comic?.thumbnail?.path}.${comic?.thumbnail?.extension}`}
            className="w-100"
            style={{
              maxHeight: "250px",
              objectFit: "cover",
            }}
          />
        </Link>
        <Card.Body
          style={{
            minHeight: "250px",
            maxHeight: "250px",
            overflowY: "auto",
          }}
        >
          <Card.Title>{comic?.title}</Card.Title>
          <Card.Text>{comic?.description?.trim() || "No description"}</Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">
            {comic?.creators?.items.map((creator) => `${creator.name} `)}
          </small>
        </Card.Footer>
      </Card>
    </div>
  );
}

comicItem.propTypes = {
  comic: PropTypes.object.isRequired,
};
export default comicItem;

import styled from "styled-components";

// eslint-disable-next-line import/prefer-default-export
export const Title = styled.h2`
  margin-left: 12px;

  @media (max-width: 724px) {
    margin-left: unset;
    font-size: 18px;
    text-align: center;
  }
`;

import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchCcomics } from "../../actions/comicsAction";
import CardDeck from "react-bootstrap/CardDeck";
import Pagination from "react-bootstrap/Pagination";
import { Title } from "./styles";

import * as Components from "../../components";

const ComicsList = () => {
  const { comics, fetching } = useSelector((state) => state.comics);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchCcomics());
  }, [dispatch]);

  const getComicsByOffset = (offset) => {
    dispatch(fetchCcomics(offset));
  };

  const renderComics = () => {
    return (
      <>
        {comics?.data?.results.map((comic) => {
          return <Components.ComicItem comic={comic} key={comic.id} />;
        })}
      </>
    );
  };
  return (
    <CardDeck>
      <div className="col-md-12">
        <Title>Comics List</Title>
      </div>
      {fetching ? <Components.ComicListSkeleton /> : renderComics()}

      <div className="col-md-12 mt-4">
        <Pagination className="d-flex justify-content-center">
          {comics?.data?.offset - 3 > 0 && (
            <Pagination.Item
              onClick={() => {
                getComicsByOffset(comics?.data.offset - 3);
              }}
            >
              {comics?.data?.offset - 3}
            </Pagination.Item>
          )}
          {comics?.data?.offset - 2 > 0 && (
            <Pagination.Item
              onClick={() => {
                getComicsByOffset(comics?.data.offset - 2);
              }}
            >
              {comics?.data?.offset - 2}
            </Pagination.Item>
          )}
          {comics?.data?.offset - 1 > 0 && (
            <Pagination.Item
              onClick={() => {
                getComicsByOffset(comics?.data.offset - 1);
              }}
            >
              {comics?.data?.offset - 1}
            </Pagination.Item>
          )}
          <Pagination.Item active>{comics?.data?.offset}</Pagination.Item>
          <Pagination.Item
            onClick={() => {
              getComicsByOffset(comics?.data.offset + 1);
            }}
          >
            {comics?.data?.offset + 1}
          </Pagination.Item>
          <Pagination.Item
            onClick={() => {
              getComicsByOffset(comics?.data.offset + 2);
            }}
          >
            {comics?.data?.offset + 2}
          </Pagination.Item>
          <Pagination.Item
            onClick={() => {
              getComicsByOffset(comics?.data.offset + 3);
            }}
          >
            {comics?.data?.offset + 3}
          </Pagination.Item>
        </Pagination>
      </div>
    </CardDeck>
  );
};

export default ComicsList;

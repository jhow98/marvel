import React from "react";
import Skeleton from "react-loading-skeleton";

const skeletonLenth = [0, 1, 2, 3];

const ComicListSkeleton = () => {
  const renderSkeleton = () => {
    return (
      <>
        {skeletonLenth.map((item) => {
          return (
            <div className="col-sm-12 col-md-6 col-lg-3 mt-3" key={item}>
              <Skeleton count={1} height={250} />
              <Skeleton
                count={1}
                style={{ marginTop: "50px", marginBottom: "25px" }}
              />
              <Skeleton count={7} />
            </div>
          );
        })}
      </>
    );
  };

  return renderSkeleton();
};

export default ComicListSkeleton;

import React from "react";
import Skeleton from "react-loading-skeleton";
import { Container, Row, Col } from "react-bootstrap";

const ComicDetailsSkeleton = () => {
  const renderSkeleton = () => {
    return (
      <Container>
        <Row className="mt-5">
          <Col xs={12} md={8}>
            <Skeleton count={4} />
          </Col>
          <Col xs={12} md={4}>
            <Skeleton circle={true} height={200} width={200} />
          </Col>

          <Col xs={12} md={2} className="mt-4">
            <Skeleton count="1"/>
          </Col>

          <Col xs={12} className="mt-1">
            <Skeleton count="1"/>
          </Col>

          <Col xs={12} style={{
            marginTop: '100px'
          }}>
            <Skeleton count="1" height={200}/>
          </Col>


        </Row>
      </Container>
    );
  };

  return renderSkeleton();
};

export default ComicDetailsSkeleton;

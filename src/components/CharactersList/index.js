import React, { useEffect } from "react";
import CardDeck from "react-bootstrap/CardDeck";
import { fetchSearchCharacters } from "../../actions/searchCharactersAction";
import { useDispatch, useSelector } from "react-redux";
import * as Components from "../../components";
import { Title } from "./styles";

const CharactersList = ({ keyword }) => {
  const dispatch = useDispatch();
  const { characters, fetched } = useSelector((state) => state.characters);

  useEffect(() => {
    if (keyword.trim()) {
      dispatch(fetchSearchCharacters(keyword));
    }
  }, [dispatch, keyword]);

  const renderCharacters = () => {
    return (
      <>
        {characters?.data?.results.map((character) => {
          return (
            <Components.CharacterItem
              character={character}
              key={character.id}
            />
          );
        })}
      </>
    );
  };
  return (
    <>
      <CardDeck className="my-5">
        <div className="col-md-12">
          <Title>Search results for "{keyword}"</Title>
        </div>
        {fetched ? renderCharacters() : <Components.ComicListSkeleton />}
      </CardDeck>

      <hr />
    </>
  );
};

export default CharactersList;

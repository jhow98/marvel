import React, { useState } from "react";
import Form from "react-bootstrap/Form";

import * as Components from "../index";

const SearchForm = () => {
  const handleInputChange = (event) => updateSearchText(event.target.value);
  const [searchText, updateSearchText] = useState("");

  const handleFormSubmit = (event) => {
    event.preventDefault();
  };

  return (
    <>
      <div className="justify-content-center">
        <Form onSubmit={handleFormSubmit}>
          <Form.Group className="d-flex flex-column mb-3 col-md-12">
            <Form.Label className="text-align-center d-flex align-self-center text-black h3 pb-2 mt-3">
              Search for a character
            </Form.Label>
            <div className="d-flex">
              <Form.Control
                value={searchText}
                onChange={handleInputChange}
                size="lg"
                type="text"
                placeholder="Search by name"
                style={{
                  width: "400px",
                  maxWidth: "80%",
                  display: "block",
                  margin: "0 auto",
                }}
              />
            </div>
          </Form.Group>
        </Form>
      </div>
      {searchText.trim() && <Components.CharactersList keyword={searchText} />}
    </>
  );
};

export default SearchForm;

import * as CSS_COLORS from './variables';
import * as API_CONSTANTS from './api_consts';

export default {
    CSS_COLORS,
    API_CONSTANTS
}


import axios from 'axios';
import constants from "../utils/index";

const api = axios.create({
    baseURL: constants.API_CONSTANTS.MARVEL_API_BASE_URL,
});

export default api;
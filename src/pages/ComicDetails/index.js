import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchComicDetails } from "../../actions/comicDetailsAction";
import {
  Carousel,
  Container,
  Row,
  Jumbotron,
  Col,
  Image,
  Badge,
} from "react-bootstrap";
import * as Components from "../../components";
import notFound from "./../../img/404.jpg";

function ComicDetails(props) {
  const { id = null } = props?.match?.params || {};

  const { comic, fetched, error } = useSelector((state) => state.comic);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchComicDetails(id));
  }, [dispatch, id]);

  const renderSkeleton = () => {
    return <Components.ComicDetailsSkeleton />;
  };
  const renderError = () => {
    return (
      <Image
        src={notFound}
        style={{
          width: "100%",
          height: "auto",
          objectFit: "cover",
        }}
        className="mt-4"
      />
    );
  };

  const renderComicDetails = () => {
    return (
      <>
        <Jumbotron className="mt-5">
          <Container>
            <Row>
              <Col xs={12} md={8}>
                <h1>{comic?.data?.results?.map((item) => item.title)}</h1>
                <p>
                  {comic?.data?.results?.map(
                    (item) =>
                      item.description?.trim() || "No description provided"
                  )}
                </p>
              </Col>
              <Col xs={12} md={4}>
                <Image
                  src={`${comic?.data?.results[0]?.thumbnail?.path}.${comic?.data?.results[0]?.thumbnail?.extension}`}
                  roundedCircle
                  style={{
                    width: "200px",
                    height: "200px",
                    objectFit: "cover",
                  }}
                />
              </Col>
            </Row>
            <small className="text-muted">
              <hr />
              <h3>Creators:</h3>
              {comic?.data?.results[0]?.creators?.items.map((creator) => {
                return (
                  <span
                    style={{
                      fontSize: "20px",
                    }}
                  >
                    <Badge variant="secondary" className="mr-2">
                      {creator.name}
                    </Badge>
                  </span>
                );
              })}
            </small>
          </Container>
        </Jumbotron>

        <Carousel className="mb-4">
          {comic?.data?.results[0]?.images?.map((img) => {
            return (
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={`${img.path}.${img.extension}`}
                  alt=""
                  style={{
                    maxHeight: "400px",
                    objectFit: "contain",
                  }}
                />
              </Carousel.Item>
            );
          })}
        </Carousel>
      </>
    );
  };

  return (
    <Components.Layout>
      {error
        ? renderError()
        : fetched
        ? renderComicDetails()
        : renderSkeleton()}
    </Components.Layout>
  );
}

export default ComicDetails;

import React from "react";
import GlobalStyle from "./../styles/global";
import * as Components from "../../components";

export default function Home() {
  return (
    <Components.Layout>
      <GlobalStyle />
      <>
        <Components.SearchForm />
        <Components.ComicsList />
      </>
    </Components.Layout>
  );
}

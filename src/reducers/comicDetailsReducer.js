export default function reducer(
    state = { comic: [], fetching: false, fetched: false, error: null },
    action
  ) {
    switch (action.type) {
      case "FETCH_COMIC_DETAILS": {
        return { ...state, fetching: true };
      }
      case "FETCH_COMIC_DETAILS_REJECTED": {
        return { ...state, fetching: false, error: action.payload };
      }
      case "FETCH_COMIC_DETAILS_FULFILLED": {
        return {
          ...state,
          fetching: false,
          fetched: true,
          comic: action.payload,
        };
      }
      default: {
        return state;
      }
    }
  }
  
import { combineReducers } from "redux";

import comics from "./comicsReducer";
import characters from "./searchCharacterReducer";
import comic from "./comicDetailsReducer";

export default combineReducers({
  comics,
  characters,
  comic
});

export default function reducer(
    state = { comics: [], fetching: false, fetched: false, error: null },
    action
  ) {
    switch (action.type) {
      case "FETCH_COMICS": {
        return { ...state, fetching: true };
      }
      case "FETCH_COMICS_REJECTED": {
        return { ...state, fetching: false, error: action.payload };
      }
      case "FETCH_COMICS_FULFILLED": {
        return {
          ...state,
          fetching: false,
          fetched: true,
          comics: action.payload,
        };
      }
  
      default: {
        return state;
      }
    }
  }
  
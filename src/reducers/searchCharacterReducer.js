export default function reducer(
    state = { characters: [], fetching: false, fetched: false, error: null },
    action
  ) {
    switch (action.type) {
      case "FETCH_SEARCH_CHARACTERS": {
        return { ...state, fetching: true };
      }
      case "FETCH_SEARCH_CHARACTERS_REJECTED": {
        return { ...state, fetching: false, error: action.payload };
      }
      case "FETCH_SEARCH_CHARACTERS_FULFILLED": {
        return {
          ...state,
          fetching: false,
          fetched: true,
          characters: action.payload,
        };
      }
  
      default: {
        return state;
      }
    }
  }
  
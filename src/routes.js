import React from 'react';
import { Switch, Route } from "react-router-dom";

import Home from "./pages/Home";
import ComicDetails from "./pages/ComicDetails";

export default function Routes() {
  return( 
  <Switch>
    <Route path="/" exact component={Home} />
    <Route path="/comic/:id" exact component={ComicDetails} />
  </Switch>);
}

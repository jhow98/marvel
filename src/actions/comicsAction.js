import api from "../services/api";
import constants from "../utils/index";

export function fetchCcomics(offset = 0) {
  return function (dispatch) {
    dispatch({ type: "FETCH_COMICS" });
    api
      .get(
        `comics?ts=100&apikey=${constants.API_CONSTANTS.MARVEL_API_KEY}&hash=${constants.API_CONSTANTS.MARVEL_API_HASH}&limit=10&offset=${offset}`
      )
      .then((response) => {
        dispatch({ type: "FETCH_COMICS_FULFILLED", payload: response.data });
      })
      .catch((err) => {
        dispatch({ type: "FETCH_COMICS_REJECTED", payload: err });
      });
  };
}

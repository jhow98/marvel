import api from "../services/api";
import constants from "../utils/index";

export function fetchComicDetails(keyword) {
  return function (dispatch) {
    api
      .get(
        `comics/${keyword}?ts=100&apikey=${constants.API_CONSTANTS.MARVEL_API_KEY}&hash=${constants.API_CONSTANTS.MARVEL_API_HASH}`
      )
      .then((response) => {
        dispatch({
          type: "FETCH_COMIC_DETAILS_FULFILLED",
          payload: response.data,
        });
      })
      .catch((err) => {
        dispatch({ type: "FETCH_COMIC_DETAILS_REJECTED", payload: err });
      });
  };
}

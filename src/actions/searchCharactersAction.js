import api from '../services/api';
import constants from "../utils/index";


export function fetchSearchCharacters(keyword) {
  return function (dispatch) {
    api
      .get(`characters?ts=100&apikey=${constants.API_CONSTANTS.MARVEL_API_KEY}&hash=${constants.API_CONSTANTS.MARVEL_API_HASH}&nameStartsWith=${keyword}`)
      .then((response) => {
        dispatch({ type: "FETCH_SEARCH_CHARACTERS_FULFILLED", payload: response.data });
      })
      .catch((err) => {
        dispatch({ type: "FETCH_SEARCH_CHARACTERS_REJECTED", payload: err });
      });
  };
}
